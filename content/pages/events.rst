Events
######

:hide: True

I often present at conferences. Check out my `previous talks <http://kennethreitz.com/pages/talks.html>`_ to learn more.

November 2012
-------------

- 11/09: PyCon CA (Toronto, CA)
    Presenting, *Something*
- 11/27 AWS Re:Invent (Las Vegas)
    Attending

December 2012
-------------

- 12/01 AngelHack (Paris, France)
    Attending
- 12/08 San Francisco
    Visiting

January 2013
------------

- 01/08: CodeMash (Sandusky, Ohio)
    Attending, Submitted Talks

February 2013
-------------

- 02/28: Waza (San Francisco, CA)
    Organizing, Attending