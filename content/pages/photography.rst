Photography
###########

:hide: True

I am passionate about many things, including code,  electronic music, and photography.

I specialize in candid, street, and available-light photography. My kit:

- Leica M9-P
- Voitlander Nokton 35mm f/1.2


Previously I had a fantastic Canon kit (Canon 5DmkII, 35L, 50L, 85L, and 135L) and the incredible Fujifilm x100. I found myself quite torn—I really wanted the mechanical interaction and size of the x100, with the performance and flexibility of the 5DmkII.

So, I sold everything and got the Leica M9. I couldn't be happier.

.. image:: http://farm9.staticflickr.com/8321/7961450946_d9a056a52b_b.jpg

Street Photography
------------------

If you'd like to buy or license a print, send me an email.

.. image:: http://farm6.staticflickr.com/5062/5672429067_a5b81498f1_b.jpg
.. image:: http://farm9.staticflickr.com/8180/8010900491_0b89bed71f_b.jpg
.. image:: http://farm9.staticflickr.com/8302/7839548144_c7f95f7e5d_b.jpg
.. image:: http://farm9.staticflickr.com/8473/8075986722_6fb7de3903_b.jpg
.. image:: http://farm7.staticflickr.com/6043/6360989077_2080dc6bfd_b.jpg
.. image:: http://farm8.staticflickr.com/7119/7075748083_d9704ac7dd_b.jpg
.. image:: http://farm9.staticflickr.com/8287/7773891280_7d04f9bb08_b.jpg

.. image:: http://farm3.staticflickr.com/2180/5791751963_e4c887d41e_b.jpg
.. image:: http://farm9.staticflickr.com/8285/7713484064_9053ccd3f2_b.jpg
.. image:: http://farm8.staticflickr.com/7021/6611198513_077cebc130_b.jpg

See More
--------

I publish dozens of photos a week. Check them out online:

- `Flickr <http://www.flickr.com/photos/kennethreitz/>`_
- `500px <http://500px.com/kennethreitz>`_